package org.sfans.admin.utils;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public class WebUtil {

	private WebUtil() {
	}

	public static Pageable updatePageable(final Pageable page, final int size) {
		return new PageRequest(page.getPageNumber(), size, page.getSort());
	}

}

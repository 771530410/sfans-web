package org.sfans.common.utils;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;


public class ReflectUtils {

    /**
     * 得到指定类型的指定位置的泛型实参
     *
     * @param clazz
     * @param index
     * @param <T>
     * @return
     */
    public static <T> Class<T> findParameterizedType(Class<?> clazz, int index) {
        Type parameterizedType = clazz.getGenericSuperclass();
        //CGLUB subclass target object(泛型在父类上)
        if (!(parameterizedType instanceof ParameterizedType)) {
            parameterizedType = clazz.getSuperclass().getGenericSuperclass();
        }
        if (!(parameterizedType instanceof  ParameterizedType)) {
            return null;
        }
        Type[] actualTypeArguments = ((ParameterizedType) parameterizedType).getActualTypeArguments();
        if (actualTypeArguments == null || actualTypeArguments.length == 0) {
            return null;
        }
        return (Class<T>) actualTypeArguments[0];
    }
    /**
     * 取得业务对应的DAO（XxxxDao）的class对象
     * @param clazz
     * @return
     */
    public static <T> Class<T> findInterfaceType(Class<?> clazz){
    	Class<?>[] interfaces = clazz.getInterfaces();
    	if(interfaces.length > 0 ){
    		return (Class<T>) interfaces[0];
    	} else {
    		throw new RuntimeException("XxxxDao must be extends BaseDao!!");
    	}
    }
}
